proc calc_dihedral {atom1 atom2 atom3 atom4} { 
   set 12 [vecsub $atom2 $atom1] 
   set 21 [vecsub $atom1 $atom2] 
   set 23 [vecsub $atom3 $atom2] 
   set 32 [vecsub $atom2 $atom3] 
   set 34 [vecsub $atom4 $atom3] 
   set n1 [veccross $12 $23] 
   set n2 [veccross $23 $34] 
   set cosangle [expr [vecdot $n1 $n2] / ([veclength $n1] * [veclength $n2])] 
   set angle [expr {180 * acos($cosangle) / 3.14159265358}] 
   set sign [vecdot [veccross $n1 $n2] $23] 
   if { $sign < 0 } { 
     set angle [expr {0-$angle}] 
   } elseif { $sign == 0 } { 
     set angle 180.0 
   } 
   return $angle 
} 

### chain C ###
# Blue
#set blue [atomselect top "protein and name CA and chain C and resid 330 to 443 503 to 528"] --> 7cwm chain A
set blue [atomselect top "protein and name CA and chain C and resid 334 335 336 337 338 339 340 341 342 343 344 345 346 347 348 349 350 351 352 353 354 355 356 357 358 359 360 361 362 363 364 365 366 367 368 369 370 371 372 373 374 375 376 377 378 389 390 391 392 393 394 395 396 397 398 399 400 401 402 403 404 405 406 407 408 409 410 411 412 413 414 415 416 417 418 419 420 421 422 423 424 425 426 427 428 429 430 431 432 433 434 435 436 437 438 439 440 441 442 443 503 504 505 506 507 508 509 510 511 512 513 514 515 516 517 518 519 520 521"]
# Cyan
set cyan [atomselect top "protein and name CA and chain C and resid 44 to 53 272 to 293"]
# Green
set green [atomselect top "protein and name CA and chain C and resid 717 to 727 1047 to 1071"]
# Magenta
#set magenta [atomselect top "protein and name CA and chain C and resid 323 to 329 529 to 590"] --> 7a25 chain B
set magenta [atomselect top "protein and name CA and chain C and resid 323 324 325 326 327 328 329 529 530 531 532 533 534 535 536 537 538 539 540 541 542 543 544 545 546 547 548 549 550 551 552 553 554 555 556 557 558 559 560 561 562 563 564 565 566 567 568 569 570 571 572 573 574 575 576 577 584 585 586 587 588 589 590"]
# Purple
#set purple [atomselect top "protein and name CA and chain C and resid 294 to 322 591 to 696"] --> 7a25 chain B
set purple [atomselect top "protein and name CA and chain C and resid 294 295 296 297 298 299 300 301 302 303 304 305 306 307 308 309 310 311 312 313 314 315 316 317 318 319 320 321 322 591 592 593 594 595 596 597 598 599 600 601 602 603 604 605 606 607 608 609 610 611 612 613 614 615 616 617 618 619 620 641 642 643 644 645 646 647 648 649 650 651 652 653 654 655 656 657 658 659 660 661 662 663 664 665 666 667 668 669 670 671 672 673 674 691 692 693 694 695 696"]
# White
#set white [atomselect top "protein and name CA and chain C and resid 27 to 43 54 to 271"] --> 7a4n chain A
set white [atomselect top "protein and name CA and chain C and resid 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 101 102 103 104 105 106 107 108 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 168 169 170 171 172 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 263 264 265 266 267 268 269 270 271"]
# Red
#set red [atomselect top "protein and name CA and chain C and resid 711 to 716 1072 to 1122"] --> 6zp5 chain C
set red [atomselect top "protein and name CA and chain C and resid 711 712 713 714 715 716 1072 1073 1074 1075 1076 1077 1078 1079 1080 1081 1082 1083 1084 1085 1086 1087 1088 1089 1090 1091 1092 1093 1094 1095 1096 1097 1098 1099 1100 1101 1102 1103 1104 1105 1106 1107 1108 1109 1110 1111 1112 1113 1114 1115 1116 1117 1118 1119 1120 1121"]
# White cluster
#set white_c [atomselect top "protein and name CA and chain C and resid 116 to 129 169 to 172"] --> 6x79 chain A
set white_c [atomselect top "protein and name CA and chain C and resid 116 117 118 119 120 121 122 125 126 127 128 129 169 170 171 172"]
# Blue cluster
set blue_c [atomselect top "protein and name CA and chain C and resid 403 to 410"]

# Get number of atoms
# Measure centers
set b_num [$blue num]
set c_num [$cyan num]
set g_num [$green num]
set m_num [$magenta num]
set p_num [$purple num]
set w_num [$white num]
set r_num [$red num]
set wst_num [$white_c num]
set bst_num [$blue_c num]

# Measure centers
set b [measure center $blue]
set c [measure center $cyan]
set g [measure center $green]
set m [measure center $magenta]
set p [measure center $purple]
set w [measure center $white]
set r [measure center $red]
set wst [measure center $white_c]
set bst [measure center $blue_c]

# Create vectors.
set vec_wc [vecsub $c $w]
set vec_cp [vecsub $p $c]
set vec_pm [vecsub $m $p]
set vec_pr [vecsub $r $p]
set vec_rp [vecsub $p $r]
set vec_mb [vecsub $b $m]
set vec_rg [vecsub $g $r]
set vec_mp [vecsub $p $m]
set vec_wstw [vecsub $w $wst]
set vec_bbst [vecsub $bst $b]

# Measure vector length.
set vec_wc_mag [veclength $vec_wc]
set vec_cp_mag [veclength $vec_cp]
set vec_pm_mag [veclength $vec_pm]
set vec_pr_mag [veclength $vec_pr]
set vec_rp_mag [veclength $vec_rp]
set vec_mb_mag [veclength $vec_mb]
set vec_rg_mag [veclength $vec_rg]
set vec_mp_mag [veclength $vec_mp]
set vec_wstw_mag [veclength $vec_wstw]
set vec_bbst_mag [veclength $vec_bbst]

# Measure angles.
set angle_wstwc [expr 57.2957795 * [expr acos([vecdot [vecnorm $vec_wstw] [vecnorm $vec_wc]])]]
set angle_wcp [expr 57.2957795 * [expr acos([vecdot [vecnorm $vec_wc] [vecnorm $vec_cp]])]]
set angle_cpm [expr 57.2957795 * [expr acos([vecdot [vecnorm $vec_cp] [vecnorm $vec_pm]])]]
set angle_cpr [expr 57.2957795 * [expr acos([vecdot [vecnorm $vec_cp] [vecnorm $vec_pr]])]]
set angle_rpm [expr 57.2957795 * [expr acos([vecdot [vecnorm $vec_rp] [vecnorm $vec_pm]])]]
set angle_pmb [expr 57.2957795 * [expr acos([vecdot [vecnorm $vec_pm] [vecnorm $vec_mb]])]]
set angle_mbbst [expr 57.2957795 * [expr acos([vecdot [vecnorm $vec_mb] [vecnorm $vec_bbst]])]]
set angle_prg [expr 57.2957795 * [expr acos([vecdot [vecnorm $vec_pr] [vecnorm $vec_rg]])]]

# Measure dihedrals
set wstwcp_dihedral [calc_dihedral $wst $w $c $p]
set wcpm_dihedral [calc_dihedral $w $c $p $m]
set wcpr_dihedral [calc_dihedral $w $c $p $r]
set cpmb_dihedral [calc_dihedral $c $p $m $b]
set pmbbst_dihedral [calc_dihedral $p $m $b $bst]
set mprg_dihedral [calc_dihedral $m $p $r $g]
set cprg_dihedral [calc_dihedral $c $p $r $g]

set output [open "VMD_out.txt" w]
puts $output "$vec_wc_mag,$vec_cp_mag,$vec_pm_mag,$vec_pr_mag,$vec_mb_mag,$vec_rg_mag,$vec_wstw_mag,$vec_bbst_mag,$angle_wstwc,$angle_wcp,$angle_cpm,$angle_cpr,$angle_rpm,$angle_pmb,$angle_mbbst,$angle_prg,$wstwcp_dihedral,$wcpm_dihedral,$wcpr_dihedral,$cpmb_dihedral,$pmbbst_dihedral,$mprg_dihedral,$cprg_dihedral"
close $output

quit

# Visulization
draw delete all
draw color red3
draw cylinder $w [vecadd $w $vec_wc]
draw sphere $w radius 2
draw cylinder $c [vecadd $c $vec_cp]
draw sphere $c radius 2
draw cylinder $p [vecadd $p $vec_pm]
draw sphere $p radius 2
draw cylinder $p [vecadd $p $vec_pr]
draw sphere $m radius 2
draw cylinder $m [vecadd $m $vec_mb]
draw sphere $r radius 2
draw cylinder $r [vecadd $r $vec_rg]
draw sphere $wst radius 2
draw cylinder $wst [vecadd $wst $vec_wstw]
draw sphere $b radius 2
draw cylinder $b [vecadd $b $vec_bbst]
draw sphere $g radius 2
draw sphere $bst radius 2

