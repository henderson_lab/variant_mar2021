proc calc_dihedral {atom1 atom2 atom3 atom4} { 
   set 12 [vecsub $atom2 $atom1] 
   set 21 [vecsub $atom1 $atom2] 
   set 23 [vecsub $atom3 $atom2] 
   set 32 [vecsub $atom2 $atom3] 
   set 34 [vecsub $atom4 $atom3] 
   set n1 [veccross $12 $23] 
   set n2 [veccross $23 $34] 
   set cosangle [expr [vecdot $n1 $n2] / ([veclength $n1] * [veclength $n2])] 
   set angle [expr {180 * acos($cosangle) / 3.14159265358}] 
   set sign [vecdot [veccross $n1 $n2] $23] 
   if { $sign < 0 } { 
     set angle [expr {0-$angle}] 
   } elseif { $sign == 0 } { 
     set angle 180.0 
   } 
   return $angle 
} 

### chain C ###
# RBDs
set rbd1 [atomselect top "protein and name CA and chain A and resid 334 335 336 337 338 339 340 341 342 343 344 345 346 347 348 349 350 351 352 353 354 355 356 357 358 359 360 361 362 363 364 365 366 367 368 369 370 371 372 373 374 375 376 377 378 389 390 391 392 393 394 395 396 397 398 399 400 401 402 403 404 405 406 407 408 409 410 411 412 413 414 415 416 417 418 419 420 421 422 423 424 425 426 427 428 429 430 431 432 433 434 435 436 437 438 439 440 441 442 443 503 504 505 506 507 508 509 510 511 512 513 514 515 516 517 518 519 520 521"]
set rbd2 [atomselect top "protein and name CA and chain B and resid 334 335 336 337 338 339 340 341 342 343 344 345 346 347 348 349 350 351 352 353 354 355 356 357 358 359 360 361 362 363 364 365 366 367 368 369 370 371 372 373 374 375 376 377 378 389 390 391 392 393 394 395 396 397 398 399 400 401 402 403 404 405 406 407 408 409 410 411 412 413 414 415 416 417 418 419 420 421 422 423 424 425 426 427 428 429 430 431 432 433 434 435 436 437 438 439 440 441 442 443 503 504 505 506 507 508 509 510 511 512 513 514 515 516 517 518 519 520 521"]
set rbd3 [atomselect top "protein and name CA and chain C and resid 334 335 336 337 338 339 340 341 342 343 344 345 346 347 348 349 350 351 352 353 354 355 356 357 358 359 360 361 362 363 364 365 366 367 368 369 370 371 372 373 374 375 376 377 378 389 390 391 392 393 394 395 396 397 398 399 400 401 402 403 404 405 406 407 408 409 410 411 412 413 414 415 416 417 418 419 420 421 422 423 424 425 426 427 428 429 430 431 432 433 434 435 436 437 438 439 440 441 442 443 503 504 505 506 507 508 509 510 511 512 513 514 515 516 517 518 519 520 521"]

# NTDs
set ntd1 [atomselect top "protein and name CA and chain A and resid 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 101 102 103 104 105 106 107 108 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 168 169 170 171 172 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 263 264 265 266 267 268 269 270 271"]
set ntd2 [atomselect top "protein and name CA and chain B and resid 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 101 102 103 104 105 106 107 108 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 168 169 170 171 172 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 263 264 265 266 267 268 269 270 271"]
set ntd3 [atomselect top "protein and name CA and chain C and resid 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 101 102 103 104 105 106 107 108 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 168 169 170 171 172 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 263 264 265 266 267 268 269 270 271"]

# RBD Refs
set rbd_c1 [atomselect top "protein and name CA and chain A and resid 403 to 410"]
set rbd_c2 [atomselect top "protein and name CA and chain B and resid 403 to 410"]
set rbd_c3 [atomselect top "protein and name CA and chain C and resid 403 to 410"]


# Measure centers
set rbd1_cent [measure center $rbd1]
set rbd2_cent [measure center $rbd2]
set rbd3_cent [measure center $rbd3]

set ntd1_cent [measure center $ntd1]
set ntd2_cent [measure center $ntd2]
set ntd3_cent [measure center $ntd3]

set rbd_c1_cent [measure center $rbd_c1]
set rbd_c2_cent [measure center $rbd_c2]
set rbd_c3_cent [measure center $rbd_c3]

# Create vectors.
set vec_r1_r2 [vecsub $rbd1_cent $rbd2_cent]
set vec_r2_r3 [vecsub $rbd2_cent $rbd3_cent]
set vec_r3_r1 [vecsub $rbd3_cent $rbd1_cent]

set vec_r1_n2 [vecsub $rbd1_cent $ntd2_cent]
set vec_r2_n3 [vecsub $rbd2_cent $ntd3_cent]
set vec_r3_n1 [vecsub $rbd3_cent $ntd1_cent]

# Measure vector length.
set vec_r1_r2_mag [veclength $vec_r1_r2]
set vec_r2_r3_mag [veclength $vec_r2_r3]
set vec_r3_r1_mag [veclength $vec_r3_r1]

set vec_r1_n2_mag [veclength $vec_r1_n2]
set vec_r2_n3_mag [veclength $vec_r2_n3]
set vec_r3_n1_mag [veclength $vec_r3_n1]

# Measure angles.
set angle_r1r2_r2r3 [expr 57.2957795 * [expr acos([vecdot [vecnorm $vec_r1_r2] [vecnorm $vec_r2_r3]])]]
set angle_r2r3_r3r1 [expr 57.2957795 * [expr acos([vecdot [vecnorm $vec_r2_r3] [vecnorm $vec_r3_r1]])]]
set angle_r3r1_r1r2 [expr 57.2957795 * [expr acos([vecdot [vecnorm $vec_r3_r1] [vecnorm $vec_r1_r2]])]]

# Measure dihedrals
set r1r2_dihedral [calc_dihedral $rbd_c1_cent $rbd1_cent $rbd2_cent $rbd_c2_cent]
set r2r3_dihedral [calc_dihedral $rbd_c2_cent $rbd2_cent $rbd3_cent $rbd_c3_cent]
set r3r1_dihedral [calc_dihedral $rbd_c3_cent $rbd3_cent $rbd1_cent $rbd_c1_cent]


# Print output to file
set output2 [open "RBD-RBD-NTD.csv" w]
puts $output2 "$vec_r1_r2_mag,$vec_r2_r3_mag,$vec_r3_r1_mag,$vec_r1_n2_mag,$vec_r2_n3_mag,$vec_r3_n1_mag,$angle_r1r2_r2r3,$angle_r2r3_r3r1,$angle_r3r1_r1r2,$r1r2_dihedral,$r2r3_dihedral,$r3r1_dihedral"
close $output2

quit

# Visulization
draw delete all
draw color cyan2
draw cylinder $rbd2_cent [vecadd $rbd2_cent $vec_r1_r2]
draw sphere $rbd2_cent radius 2

draw color magenta2
draw cylinder $rbd3_cent [vecadd $rbd3_cent $vec_r2_r3]
draw sphere $rbd3_cent radius 2

draw color yellow2
draw cylinder $rbd1_cent [vecadd $rbd1_cent $vec_r3_r1]
draw sphere $rbd1_cent radius 2

draw color green2
draw cylinder $ntd2_cent [vecadd $ntd2_cent $vec_r1_n2]
draw sphere $ntd2_cent radius 2

draw color blue2
draw cylinder $ntd3_cent [vecadd $ntd3_cent $vec_r2_n3]
draw sphere $ntd3_cent radius 2

draw color violet2
draw cylinder $ntd1_cent [vecadd $ntd1_cent $vec_r3_n1]
draw sphere $ntd1_cent radius 2







