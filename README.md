The scripts here are were used for the vector relationship and Markov modelling work in our paper "Effect of natural mutations of SARS-CoV-2 
on spike structure, conformation and antigenicity" posted on bioRxiv March 12th 2021.


The VMD scripts can be run from the tcl console after loading VMD and your structure or via the command line using: vmd -dispdev none -e script_name.tcl your_SARS-CoV-2_structure.pdb

Residue numbers in these scripts are specific to SARS-CoV-2 and must be numbered in a manner consistent with the the PDB ID 6VXX structure. Analysis of other Coronavirus Spike structures will require modification of the residue selections. Tcl commands for visulizing the vectors in VMD are provided at the bottom of each VMD script.

The HTMD.py script commands can be run in an interactive python session to allow for modification of the model and/or testing alternative projections, etc. This requires installation of HTMD (https://software.acellera.com/install-htmd.html)

Filtered trajectories for both the WT and Mut (K417N+E484K+N501Y) datasets are available through Globus (https://www.globus.org/) via the following link: https://app.globus.org/file-manager?origin_id=aa7c9c4c-8343-11eb-b799-f57b2d55370d&origin_path=%2F
