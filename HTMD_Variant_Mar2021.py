from htmd.ui import *
config(njobs=-2)

fsims = simlist(glob('./filtered/*/'), './filtered/filtered.pdb')  

refer = Molecule('./filtered/filtered.pdb')

rmsd = Metric(fsims)   
rmsd.set(MetricRmsd(refmol=refer, trajrmsdstr='protein and name CA and resid 144 to 164', pbc=False))   
rmsddat = rmsd.project()   
rmsddat.fstep = 0.2

rmsddat.dropTraj()

metr = Metric(fsims)
metr.set(MetricSelfDistance('resid 145 146 147 148 149 150 151 152 153 154 158 159 160 161 162 and name CA', metric='distances', periodic=None))

metr2 = Metric(fsims)
# For WT
metr2.set(MetricDistance(sel1='protein and resid 158 and name OE1 OE2', sel2='resid 21 to 28 and element N O or resid 87 to 99 and element N O or resid 120 to 174 and element N O', groupsel1='all', metric='contacts', threshold=3.5, periodic=None))
# For Mut
#metr2.set(MetricDistance(sel1='protein and resid 158 and name NZ', sel2='resid 21 to 28 and element N O or resid 87 to 99 and element N O or resid 120 to 174 and element N O', metric='contacts', threshold=3.5, periodic=None))

data = metr.project()    
data.fstep = 0.2 

data2 = metr2.project()    
data2.fstep = 0.2 

data.dropTraj()
data2.dropTraj()

tica = TICA(data, 5, units='ns')     
datatica=tica.project(ndim=5)  
datatica.cluster(MiniBatchKMeans(n_clusters=500), mergesmall=5)
# datatica.plotCounts(dimX=0, dimY=1)
# datatica.plotClusters(dimX=0, dimY=1)


model = Model(datatica) 
# model.plotTimescales(errors='bayes')

model.markovModel(30, 2, units='ns')
# model.plotFES(0, 1, temperature=298, data=rmsdtica)

means = getStateStatistic(model, data2, states=range(model.macronum), weighted=True, method=np.mean)

states = model.getStates(numsamples=250)
j=0      
for m in states:         
	wrt = str(j)        
	wrt3 = str(wrt + '.dcd')        
	m.write(wrt3)         
	j+=1
