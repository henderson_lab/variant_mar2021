proc calc_dihedral {atom1 atom2 atom3 atom4} { 
   set 12 [vecsub $atom2 $atom1] 
   set 21 [vecsub $atom1 $atom2] 
   set 23 [vecsub $atom3 $atom2] 
   set 32 [vecsub $atom2 $atom3] 
   set 34 [vecsub $atom4 $atom3] 
   set n1 [veccross $12 $23] 
   set n2 [veccross $23 $34] 
   set cosangle [expr [vecdot $n1 $n2] / ([veclength $n1] * [veclength $n2])] 
   set angle [expr {180 * acos($cosangle) / 3.14159265358}] 
   set sign [vecdot [veccross $n1 $n2] $23] 
   if { $sign < 0 } { 
     set angle [expr {0-$angle}] 
   } elseif { $sign == 0 } { 
     set angle 180.0 
   } 
   return $angle 
} 

# New anchor resis
set SD2anch1 [atomselect top "protein and name CA and resid 671 and chain A"]
set SD1anch1 [atomselect top "protein and name CA and resid 575 and chain A"]
set NTDprimeanch1 [atomselect top "protein and name CA and resid 276 and chain A"]

set SD2anch2 [atomselect top "protein and name CA and resid 671 and chain B"]
set SD1anch2 [atomselect top "protein and name CA and resid 575 and chain B"]
set NTDprimeanch2 [atomselect top "protein and name CA and resid 276 and chain B"]

set SD2anch3 [atomselect top "protein and name CA and resid 671 and chain C"]
set SD1anch3 [atomselect top "protein and name CA and resid 575 and chain C"]
set NTDprimeanch3 [atomselect top "protein and name CA and resid 276 and chain C"]

# Centroids
set NTDprime1 [atomselect top "protein and name CA and chain A and resid 44 to 53 272 to 293"]
set SD11 [atomselect top "protein and name CA and chain A and resid 323 324 325 326 327 328 329 529 530 531 532 533 534 535 536 537 538 539 540 541 542 543 544 545 546 547 548 549 550 551 552 553 554 555 556 557 558 559 560 561 562 563 564 565 566 567 568 569 570 571 572 573 574 575 576 577 584 585 586 587 588 589 590"]
set SD21 [atomselect top "protein and name CA and chain A and resid 294 295 296 297 298 299 300 301 302 303 304 305 306 307 308 309 310 311 312 313 314 315 316 317 318 319 320 321 322 591 592 593 594 595 596 597 598 599 600 601 602 603 604 605 606 607 608 609 610 611 612 613 614 615 616 617 618 619 620 641 642 643 644 645 646 647 648 649 650 651 652 653 654 655 656 657 658 659 660 661 662 663 664 665 666 667 668 669 670 671 672 673 674 691 692 693 694 695 696"]

set NTDprime2 [atomselect top "protein and name CA and chain B and resid 44 to 53 272 to 293"]
set SD12 [atomselect top "protein and name CA and chain B and resid 323 324 325 326 327 328 329 529 530 531 532 533 534 535 536 537 538 539 540 541 542 543 544 545 546 547 548 549 550 551 552 553 554 555 556 557 558 559 560 561 562 563 564 565 566 567 568 569 570 571 572 573 574 575 576 577 584 585 586 587 588 589 590"]
set SD22 [atomselect top "protein and name CA and chain B and resid 294 295 296 297 298 299 300 301 302 303 304 305 306 307 308 309 310 311 312 313 314 315 316 317 318 319 320 321 322 591 592 593 594 595 596 597 598 599 600 601 602 603 604 605 606 607 608 609 610 611 612 613 614 615 616 617 618 619 620 641 642 643 644 645 646 647 648 649 650 651 652 653 654 655 656 657 658 659 660 661 662 663 664 665 666 667 668 669 670 671 672 673 674 691 692 693 694 695 696"]

set NTDprime3 [atomselect top "protein and name CA and chain C and resid 44 to 53 272 to 293"]
set SD13 [atomselect top "protein and name CA and chain C and resid 323 324 325 326 327 328 329 529 530 531 532 533 534 535 536 537 538 539 540 541 542 543 544 545 546 547 548 549 550 551 552 553 554 555 556 557 558 559 560 561 562 563 564 565 566 567 568 569 570 571 572 573 574 575 576 577 584 585 586 587 588 589 590"]
set SD23 [atomselect top "protein and name CA and chain C and resid 294 295 296 297 298 299 300 301 302 303 304 305 306 307 308 309 310 311 312 313 314 315 316 317 318 319 320 321 322 591 592 593 594 595 596 597 598 599 600 601 602 603 604 605 606 607 608 609 610 611 612 613 614 615 616 617 618 619 620 641 642 643 644 645 646 647 648 649 650 651 652 653 654 655 656 657 658 659 660 661 662 663 664 665 666 667 668 669 670 671 672 673 674 691 692 693 694 695 696"]

set S2heli [atomselect top "protein and name CA and resid 1004 to 1018"]


# Measure centers
set SD2cent1 [measure center $SD21]
set SD2cent2 [measure center $SD22]
set SD2cent3 [measure center $SD23]
set SD1cent1 [measure center $SD11]
set SD1cent2 [measure center $SD12]
set SD1cent3 [measure center $SD13]
set NTDprimecent1 [measure center $NTDprime1]
set NTDprimecent2 [measure center $NTDprime2]
set NTDprimecent3 [measure center $NTDprime3]

set SD2anchcent1 [measure center $SD2anch1]
set SD2anchcent2 [measure center $SD2anch2]
set SD2anchcent3 [measure center $SD2anch3]
set SD1anchcent1 [measure center $SD1anch1]
set SD1anchcent2 [measure center $SD1anch2]
set SD1anchcent3 [measure center $SD1anch3]
set NTDprimeanchcent1 [measure center $NTDprimeanch1]
set NTDprimeanchcent2 [measure center $NTDprimeanch2]
set NTDprimeanchcent3 [measure center $NTDprimeanch3]

set S2helicent [measure center $S2heli]

# Create vectors.
set SD12NTDprime3vec [vecsub $NTDprimecent3 $SD1cent2]
set SD13NTDprime1vec [vecsub $NTDprimecent1 $SD1cent3]
set SD11NTDprime2vec [vecsub $NTDprimecent2 $SD1cent1]

set NTDprime3SD23vec [vecsub $SD2cent3 $NTDprimecent3]
set NTDprime3NTDprime3anchvec [vecsub $NTDprimeanchcent3 $NTDprimecent3]
set SD23SD13vec [vecsub $SD1cent3 $SD2cent3]
set SD23SD23anchvec [vecsub $SD2anchcent3 $SD2cent3]
set SD13SD13anchvec [vecsub $SD1anchcent3 $SD1cent3]

set NTDprime1SD21vec [vecsub $SD2cent1 $NTDprimecent1]
set NTDprime1NTDprime1anchvec [vecsub $NTDprimeanchcent1 $NTDprimecent1]
set SD21SD11vec [vecsub $SD1cent1 $SD2cent1]
set SD21SD21anchvec [vecsub $SD2anchcent1 $SD2cent1]
set SD11SD11anchvec [vecsub $SD1anchcent1 $SD1cent1]

set NTDprime2SD22vec [vecsub $SD2cent2 $NTDprimecent2]
set NTDprime2NTDprime2anchvec [vecsub $NTDprimeanchcent2 $NTDprimecent2]
set SD22SD12vec [vecsub $SD1cent2 $SD2cent2]
set SD22SD22anchvec [vecsub $SD2anchcent2 $SD2cent2]
set SD12SD12anchvec [vecsub $SD1anchcent2 $SD1cent2]

set SD2cent1vec [vecsub $S2helicent $SD2cent1]
set SD2cent2vec [vecsub $S2helicent $SD2cent2]
set SD2cent3vec [vecsub $S2helicent $SD2cent3]
set SD1cent1vec [vecsub $S2helicent $SD1cent1]
set SD1cent2vec [vecsub $S2helicent $SD1cent2]
set SD1cent3vec [vecsub $S2helicent $SD1cent3]
set NTDprimecent1vec [vecsub $S2helicent $NTDprimecent1]
set NTDprimecent2vec [vecsub $S2helicent $NTDprimecent2]
set NTDprimecent3vec [vecsub $S2helicent $NTDprimecent3]

# Measure vector length.
set SD2cent1vecmag  [veclength $SD2cent1vec]
set SD2cent2vecmag [veclength $SD2cent2vec]
set SD2cent3vecmag [veclength $SD2cent3vec]
set SD1cent1vecmag [veclength $SD1cent1vec]
set SD1cent2vecmag [veclength $SD1cent2vec]
set SD1cent3vecmag [veclength $SD1cent3vec]
set NTDprimecent1vecmag [veclength $NTDprimecent1vec]
set NTDprimecent2vecmag [veclength $NTDprimecent2vec]
set NTDprimecent3vecmag [veclength $NTDprimecent3vec]


# Measure angles.
set theta1 [expr 57.2957795 * [expr acos([vecdot [vecnorm $NTDprime3SD23vec] [vecnorm $SD23SD23anchvec]])]]
set theta2 [expr 57.2957795 * [expr acos([vecdot [vecnorm $SD23SD23anchvec] [vecnorm $SD23SD13vec]])]]

set theta3 [expr 57.2957795 * [expr acos([vecdot [vecnorm $NTDprime1SD21vec] [vecnorm $SD21SD21anchvec]])]]
set theta4 [expr 57.2957795 * [expr acos([vecdot [vecnorm $SD21SD21anchvec] [vecnorm $SD21SD11vec]])]]

set theta5 [expr 57.2957795 * [expr acos([vecdot [vecnorm $NTDprime2SD22vec] [vecnorm $SD22SD22anchvec]])]]
set theta6 [expr 57.2957795 * [expr acos([vecdot [vecnorm $SD22SD22anchvec] [vecnorm $SD22SD12vec]])]]

# Measure dihedrals
set phi1 [calc_dihedral $SD1anchcent2 $SD1cent2 $NTDprimecent3 $NTDprimeanchcent3]
set phi2 [calc_dihedral $NTDprimeanchcent3 $NTDprimecent3 $SD2cent3 $SD2anchcent3]
set phi3 [calc_dihedral $SD2anchcent3 $SD2cent3 $SD1cent3 $SD1anchcent3]

set phi4 [calc_dihedral $SD1anchcent3 $SD1cent3 $NTDprimecent1 $NTDprimeanchcent1]
set phi5 [calc_dihedral $NTDprimeanchcent1 $NTDprimecent1 $SD2cent1 $SD2anchcent1]
set phi6 [calc_dihedral $SD2anchcent1 $SD2cent1 $SD1cent1 $SD1anchcent1]

set phi7 [calc_dihedral $SD1anchcent1 $SD1cent1 $NTDprimecent2 $NTDprimeanchcent2]
set phi8 [calc_dihedral $NTDprimeanchcent2 $NTDprimecent2 $SD2cent2 $SD2anchcent2]
set phi9 [calc_dihedral $SD2anchcent2 $SD2cent2 $SD1cent2 $SD1anchcent2]

# Produce Output
set output2 [open "Communication.csv" w]
puts $output2 "$theta1,$theta2,$theta3,$theta4,$theta5,$theta6,$phi1,$phi2,$phi3,$phi4,$phi5,$phi6,$phi7,$phi8,$phi9,$SD2cent1vecmag,$SD2cent2vecmag,$SD2cent3vecmag,$SD1cent1vecmag,$SD1cent2vecmag,$SD1cent3vecmag,$NTDprimecent1vecmag,$NTDprimecent2vecmag,$NTDprimecent3vecmag"
close $output2

quit

# Visulization
draw delete all
draw color cyan2
draw cylinder $SD1cent2 [vecadd $SD1cent2 $SD12NTDprime3vec]
draw sphere $SD1cent2 radius 2
draw cylinder $SD1cent3 [vecadd $SD1cent3 $SD13NTDprime1vec]
draw sphere $SD1cent3 radius 2
draw cylinder $SD1cent1 [vecadd $SD1cent1 $SD11NTDprime2vec]
draw sphere $SD1cent1 radius 2

draw color red3
draw cylinder $NTDprimecent3 [vecadd $NTDprimecent3 $NTDprime3SD23vec]
draw sphere $NTDprimecent3 radius 2
draw color violet
draw cylinder $NTDprimecent3 [vecadd $NTDprimecent3 $NTDprime3NTDprime3anchvec]
draw sphere $NTDprimecent3 radius 2
draw color blue2
draw cylinder $SD2cent3 [vecadd $SD2cent3 $SD23SD13vec]
draw sphere $SD2cent3 radius 2
draw color yellow
draw cylinder $SD2cent3 [vecadd $SD2cent3 $SD23SD23anchvec]
draw sphere $SD2cent3 radius 2
draw color lime
draw cylinder $SD1cent3 [vecadd $SD1cent3 $SD13SD13anchvec]
draw sphere $SD1cent3 radius 2

draw color red3
draw cylinder $NTDprimecent1 [vecadd $NTDprimecent1 $NTDprime1SD21vec]
draw sphere $NTDprimecent1 radius 2
draw color violet
draw cylinder $NTDprimecent1 [vecadd $NTDprimecent1 $NTDprime1NTDprime1anchvec]
draw sphere $NTDprimecent1 radius 2
draw color blue2
draw cylinder $SD2cent1 [vecadd $SD2cent1 $SD21SD11vec]
draw sphere $SD2cent1 radius 2
draw color yellow
draw cylinder $SD2cent1 [vecadd $SD2cent1 $SD21SD21anchvec]
draw sphere $SD2cent1 radius 2
draw color lime
draw cylinder $SD1cent1 [vecadd $SD1cent1 $SD11SD11anchvec]
draw sphere $SD1cent1 radius 2

draw color red3
draw cylinder $NTDprimecent2 [vecadd $NTDprimecent2 $NTDprime2SD22vec]
draw sphere $NTDprimecent2 radius 2
draw color violet
draw cylinder $NTDprimecent2 [vecadd $NTDprimecent2 $NTDprime2NTDprime2anchvec]
draw sphere $NTDprimecent2 radius 2
draw color blue2
draw cylinder $SD2cent2 [vecadd $SD2cent2 $SD22SD12vec]
draw sphere $SD2cent2 radius 2
draw color yellow
draw cylinder $SD2cent2 [vecadd $SD2cent2 $SD22SD22anchvec]
draw sphere $SD2cent2 radius 2
draw color lime
draw cylinder $SD1cent2 [vecadd $SD1cent2 $SD12SD12anchvec]
draw sphere $SD1cent2 radius 2

draw color orange
draw sphere $SD2anchcent1 radius 2
draw sphere $SD2anchcent2 radius 2
draw sphere $SD2anchcent3 radius 2

draw sphere $SD1anchcent1 radius 2
draw sphere $SD1anchcent2 radius 2
draw sphere $SD1anchcent3 radius 2

draw sphere $NTDprimeanchcent1 radius 2
draw sphere $NTDprimeanchcent2 radius 2
draw sphere $NTDprimeanchcent3 radius 2

draw color purple
draw sphere $S2helicent radius 2

draw color white
draw cylinder $SD2cent1 [vecadd $SD2cent1 $SD2cent1vec]
draw cylinder $SD2cent2 [vecadd $SD2cent2 $SD2cent2vec]
draw cylinder $SD2cent3 [vecadd $SD2cent3 $SD2cent3vec]
draw cylinder $SD1cent1 [vecadd $SD1cent1 $SD1cent1vec]
draw cylinder $SD1cent2 [vecadd $SD1cent2 $SD1cent2vec]
draw cylinder $SD1cent3 [vecadd $SD1cent3 $SD1cent3vec]
draw cylinder $NTDprimecent1 [vecadd $NTDprimecent1 $NTDprimecent1vec]
draw cylinder $NTDprimecent2 [vecadd $NTDprimecent2 $NTDprimecent2vec]
draw cylinder $NTDprimecent3 [vecadd $NTDprimecent3 $NTDprimecent3vec]
